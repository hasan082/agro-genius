import 'package:flutter/material.dart';

import 'color_constants.dart';

double welcomeTitleTextSize = 20.0;
double welcomeTextSize = 18.0;
double buttonTextSize = 17.0;

double labelSize = 14.0;

double mediumPadding = 15.0;

double elevation = 3;

double smallHeightGap = 5.0;
double mediumHeightGap = 15.0;
double longHeightGap = 25.0;

double bodyGapScrollView = 15.0;

double aboveImageHeight = 150.0;
double borderRadius = 6.0;

double iconSize = 18.0;
double listViewFontSize = 17.0;


double bottomBarItemWidth = 40.0; //BOTTOM BAR ITEM WIDTH==============

TextStyle headlineTextSizeMedium = const TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);

TextStyle headlineTextSizeSmall = const TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
);

TextStyle mediumTextSize = const TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.w500,
);

TextStyle semiMediumTextSize = const TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.w500,
);
TextStyle smallTextSize = const TextStyle(
  fontSize: 15.0,
  fontWeight: FontWeight.w500,
);


TextStyle smallProfileTextSize = TextStyle(
  fontSize: 14.0,
  color: grayShadowTextColor,
);

TextStyle mediumProfileTextSize = TextStyle(
  fontSize: 18.0,
  color: grayShadowTextColor,
);



