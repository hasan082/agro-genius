import 'package:flutter/material.dart';


String welcomeText1 = "Watering without worry";
String welcomeText2 = "Get Information About Your Plants";
String welcomeText3 = "Build & Share with Community";
String welcomeDescription1 = 'You can set your schedule watering plant properly and can exchange schedule automatically if come a bad climate';
String welcomeDescription2 = 'You can scan information of your plant or a pest that harm your plant and get information how to take care that problems.';
String welcomeDescription3 = 'You can create group or community of your garden or community in your city. And you can also trade, lend, buy, or share any goods with other people';

String dashBoard = "DashBoard";
String rememberMe = "Remember me";
String forgotPassword = "Forgot password?";
String forgotPageText = "Fill your email and we will sent you a link to change your password";
String disease = 'The diagnosis of the disease could not be made as the image you have provided is either not of a plant or is too blurry. Please try again with a clear image of a plant.';

