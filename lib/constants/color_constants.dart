import 'package:flutter/material.dart';

Color bgColor = const Color(0xFFf5f5f5);
Color bgColor2 = const Color(0xFFfbfbfb);
Color mainColor  = const Color(0xFF3ED400);
Color inputLabelColor  = Colors.grey;
Color whiteColor  = Colors.white;
Color textButtonColor  = const Color(0xFF3ED400);
Color textColor  = Colors.black;
Color? shadowColor= Colors.grey[300];
Color? grayShadowTextColor = Colors.grey[600];

