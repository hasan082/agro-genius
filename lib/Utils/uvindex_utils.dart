class UvIndexUtils{
  String getUVRiskLevel(double uvIndex) {
    if (uvIndex >= 0 && uvIndex <= 2) {
      return 'Very Week';
    } else if (uvIndex >= 3 && uvIndex <= 5) {
      return 'Moderate';
    } else if (uvIndex >= 6 && uvIndex <= 9) {
      return 'High';
    } else if (uvIndex >= 10 && uvIndex < 12) {
      return 'Very High';
    } else {
      return 'Extreme';
    }
  }

}