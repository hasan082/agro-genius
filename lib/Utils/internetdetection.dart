import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';

class InternetConnectionChecker {
  Future<bool> checkInternetConnection() async {
    final connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      try {
        final response = await InternetAddress.lookup('google.com');
        if (response.isNotEmpty && response[0].rawAddress.isNotEmpty) {
          return true;
        }
      } on Exception catch (_) {
        return false;
      }
    }
    return false;
  }
}
