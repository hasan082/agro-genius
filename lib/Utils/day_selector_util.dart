class GetDataDay {
  static List<String> getDayNames() {
    DateTime today = DateTime.now();
    List<String> dayNames = [];
    for (int i = 0; i < 15; i++) {
      DateTime nextDate = today.add(Duration(days: i));
      String dayName = _getWeekdayName(nextDate.weekday);
      dayNames.add('$dayName, ${nextDate.day}');
    }
    return dayNames;
  }

  static String _getWeekdayName(int? weekdayNumber) {
    switch (weekdayNumber) {
      case 1:
        return 'Mon';
      case 2:
        return 'Tue';
      case 3:
        return 'Wed';
      case 4:
        return 'Thu';
      case 5:
        return 'Fri';
      case 6:
        return 'Sat';
      case 7:
        return 'Sun';
      default:
        return '';
    }
  }
}
