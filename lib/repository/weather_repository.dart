import 'package:http/http.dart';

class WeatherRepository {
  Future<Response> getMyCurrentWeatherRepo(double lat, double lon) async {
    Response response = await Client().get(
        Uri.parse(
            "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/$lat,$lon?unitGroup=metric&key=SZ27MYZY2ZSHPM6VJNG9A8HH7&include=current"
        )
    );
    return response;
  }

  // Future<Response> getMyCurrentWeatherRepo(double lat, double lon) async{
  //   Response response = await Client().get(
  //       Uri.parse(
  //           'https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=2be8b1f37693e21f54d2e071d3429e46&units=metric'));
  //   return response;
  // }



}