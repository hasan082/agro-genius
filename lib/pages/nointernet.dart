import 'package:agrogenius/constants/color_constants.dart';
import 'package:agrogenius/constants/size_constant.dart';
import 'package:flutter/material.dart';

class NoInternetScreen extends StatelessWidget {
  final VoidCallback? onRefreshPressed;

  const NoInternetScreen({Key? key, this.onRefreshPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.signal_wifi_off,
                size: 80,
                color: Colors.grey[400],
              ),
              const SizedBox(height: 16),
              const Text(
                "No internet connection",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 8),
              Text(
                "Please check your internet connection and try again.",
                style: smallTextSize,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 16),
              Container(
                margin: const EdgeInsets.only(top: 15),
                width: 140,
                height: 48,
                decoration: BoxDecoration(
                    color: mainColor,
                  borderRadius: BorderRadius.circular(6)
                ),
                child: ElevatedButton(
                  onPressed: onRefreshPressed,
                  child: const Text("Refresh", style: TextStyle(color: Colors.white, fontSize: 18,),),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
