import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../constants/text_constant.dart';
import '../widgets/button_widget.dart';
import 'welcome_screen.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  int _currentPage = 0;
  final PageController controller = PageController(initialPage: 0);
  final List<Widget> pages = [
    Container(color: Colors.red),
    Container(color: Colors.blue),
    Container(color: Colors.green),
  ];

  @override
  void initState() {
    super.initState();
    FlutterNativeSplash.remove();
    controller.addListener(() {
      setState(() {
        _currentPage = controller.page!.round();
      });
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Container(
          color: bgColor,
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: PageView(
                  controller: controller,
                  onPageChanged: (int page) {
                    setState(() {
                      _currentPage = page;
                    });
                  },
                  children: [
                    Container(
                      color: bgColor,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              width: double.maxFinite,
                              height: 150,
                              child: Image.asset(
                                'assets/images/onboarding_1.png',
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  welcomeText1,
                                  style: TextStyle(
                                      fontSize: welcomeTitleTextSize,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  welcomeDescription1,
                                  style: TextStyle(
                                    fontSize: welcomeTextSize,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: bgColor,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              width: double.maxFinite,
                              height: 150,
                              child: Image.asset(
                                'assets/images/onboarding_2.png',
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  welcomeText2,
                                  style: TextStyle(
                                      fontSize: welcomeTitleTextSize,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  welcomeDescription2,
                                  style: TextStyle(
                                    fontSize: welcomeTextSize,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: bgColor,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              width: double.maxFinite,
                              height: 150,
                              child: Image.asset(
                                'assets/images/onboarding_1.png',
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  welcomeText3,
                                  style: TextStyle(
                                      fontSize: welcomeTitleTextSize,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  welcomeDescription3,
                                  style: TextStyle(
                                    fontSize: welcomeTextSize,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  pages.length,
                  (index) => Container(
                    width: _currentPage == index ? 16.0 : 10.0,
                    height: 10.0,
                    margin: const EdgeInsets.symmetric(horizontal: 4.0),
                    decoration: BoxDecoration(
                      color: _currentPage == index
                          ? mainColor
                          : Colors.grey.shade400,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              _currentPage != 2
                  ? Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(10.0),
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Colors.white60,
                      ),
                      margin: const EdgeInsets.only(top: 20),
                      child: CustomButtonWidget(
                        onPressed: () {
                          controller.nextPage(
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.ease,
                          );
                        },
                        child: Text(
                          'Next',
                          style: TextStyle(fontSize: buttonTextSize),
                        ),
                      ),
                    )
                  : Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(10.0),
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Colors.white60,
                      ),
                      margin: const EdgeInsets.only(top: 20),
                      child: CustomButtonWidget(
                        onPressed: () {
                          Navigator.pushAndRemoveUntil(context,
                              MaterialPageRoute(builder: (context) {
                            return const WelcomeScreen();
                          }), (route) => false);
                        },
                        child: Text(
                          'Get Started',
                          style: TextStyle(fontSize: buttonTextSize),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
