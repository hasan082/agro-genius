import 'package:agrogenius/constants/color_constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../widgets/default_textfiled.dart';

class ProfileUpdateScreen extends StatefulWidget {
  const ProfileUpdateScreen({Key? key}) : super(key: key);

  @override
  State<ProfileUpdateScreen> createState() => _ProfileUpdateScreenState();
}

class _ProfileUpdateScreenState extends State<ProfileUpdateScreen> {
  TextEditingController? nameCtrl;
  TextEditingController? bioCtrl;
  TextEditingController? emailCtrl;
  TextEditingController? phoneCtrl;

  FirebaseAuth? firebaseAuth;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      appBar: AppBar(
        title: const Text('Update Profile'),
        actions: [
          IconButton(
              onPressed: (){

              },
              icon: const Icon(Icons.check, size: 30, color: Colors.white,)
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: [
            const SizedBox(height: 20),
            ReusableTextField(
              controller: nameCtrl,
              hintText: 'Update Name',
              maxLines: 1,
            ),
            const SizedBox(height: 10,),
            ReusableTextField(
              controller: bioCtrl,
              keyboardType: TextInputType.emailAddress,
              hintText: 'Update Email',
              maxLines: 1,
            ),
            const SizedBox(height: 10,),
            ReusableTextField(
              controller: phoneCtrl,
              keyboardType: TextInputType.phone,
              hintText: 'Update Phone',
              maxLines: 1,
            ),
            const SizedBox(height: 10,),
            ReusableTextField(
              controller: phoneCtrl,
              keyboardType: TextInputType.phone,
              hintText: 'Update Phone',
              maxLines: 1,
            ),
            const SizedBox(height: 10,),
            ReusableTextField(
              controller: phoneCtrl,
              keyboardType: TextInputType.phone,
              hintText: 'Update Phone',
              maxLines: 1,
            ),
          ],

        ),
      ),
    );
  }
}
