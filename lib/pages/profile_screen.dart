import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../widgets/profile_img_widget.dart';
import 'image_pick_screen.dart';

class ProfileScreen extends StatefulWidget {
  final String? imageUrl;

  const ProfileScreen({Key? key, this.imageUrl}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Widget personalInfo(IconData icon, String infoTxt) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(icon, size: 24, color: mainColor),
        const SizedBox(width: 6),
        Text(infoTxt, style: mediumTextSize),
      ],
    );
  }

  Widget postLikeCount(IconData icon, String charName) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(icon, size: 24, color: mainColor),
        const SizedBox(height: 6),
        Text(charName, style: smallTextSize),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          const SizedBox(height: 20),
          ProfileWidget(
            isEdit: true,
            imagePath: widget.imageUrl?.isNotEmpty == true ? widget.imageUrl! : 'https://picsum.photos/200',
            onClicked: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const ImagePickScreen();
              }));
            },
          ),
          const SizedBox(height: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("User Name", style: mediumTextSize),
              const SizedBox(height: 5),
              Text("Bio", style: smallTextSize),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  postLikeCount(Icons.article, "Post"),
                  postLikeCount(Icons.favorite, "Like"),
                ],
              ),
              const SizedBox(height: 15),
              personalInfo(Icons.email, "your@email.com"),
              const SizedBox(height: 10),
              personalInfo(Icons.phone, "555-555-5555"),
            ],
          ),
          Divider(height: 40, color: mainColor),
          Center(child: Text("All Post", style: mediumTextSize)),
          Divider(height: 40, color: mainColor),
          GridView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
            ),
            itemCount: 9,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                ),
                child: Center(child: Text('ItemNo $index')),
              );
            },
          ),
        ],
      ),
    );
  }
}
