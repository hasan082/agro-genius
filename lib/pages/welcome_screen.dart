import 'package:agrogenius/pages/register_screen.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../widgets/button_widget.dart';
import '../widgets/top_icon_widget.dart';
import 'login_screen.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Stack(children: [
          const TopIconWidget(),
          Container(
            padding: const EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  width: double.maxFinite,
                  height: 130,
                  child: Image.asset(
                    'assets/images/welcome.png',
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),
                const Text(
                  'Welcome to Agro Genius',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 25,
                ),
                CustomButtonWidget(
                  child:
                      Text('Login', style: TextStyle(fontSize: buttonTextSize)),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const LoginScreen();
                    }));
                  },
                ),
                const SizedBox(
                  height: 25,
                ),
                CustomButtonWidget(
                  child: Text(
                    'Register',
                    style: TextStyle(fontSize: buttonTextSize),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const RegisterScreen();
                    }));
                  },
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
