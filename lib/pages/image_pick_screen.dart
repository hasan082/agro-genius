import 'dart:io';
import 'package:agrogenius/constants/size_constant.dart';
import 'package:agrogenius/widgets/bottom_navigation_widget.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;
import '../constants/color_constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ImagePickScreen extends StatefulWidget {
  const ImagePickScreen({Key? key, this.imageUrl}) : super(key: key);

  final String? imageUrl;

  @override
  State<ImagePickScreen> createState() => _ImagePickScreenState();
}
class _ImagePickScreenState extends State<ImagePickScreen> {
  final FirebaseStorage _storage = FirebaseStorage.instance;
  late File _imageFile = File('');
  File? _selectedImage;
  String? _uploadedImageUrl;
  final imgPicker = ImagePicker();


  Future<void> _uploadImageToFirebase() async {
    if (_selectedImage != null) {
      try {
        String fileName = Path.basename(_selectedImage!.path);
        Reference reference = _storage.ref().child('images/$fileName');
        UploadTask uploadTask = reference.putFile(_selectedImage!);
        TaskSnapshot taskSnapshot = await uploadTask;
        String downloadUrl = await taskSnapshot.ref.getDownloadURL();

        setState(() {
          _uploadedImageUrl = downloadUrl;
        });
      } catch (e) {
        print('Error uploading image to Firebase: $e');
      }
    }
  }
  Future<void> _pickImage(ImageSource source) async {
    final pickedImage = await ImagePicker().pickImage(source: source);

    if (pickedImage != null) {
      setState(() {
        _selectedImage = File(pickedImage.path);
      });

      // Upload the selected image to Firebase Storage
      await _uploadImageToFirebase();

      // Navigate to the other page with the uploaded image URL
      if(context.mounted){
        // const CustomBottomNav(feedIndex: 3);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CustomBottomNav(feedIndex: 3, imageUrl: _uploadedImageUrl),
          ),
        );
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => ProfileScreen(imageUrl: _uploadedImageUrl),
        //   ),
        // );
      }

    }
  }
  // Future<void> _getImage(ImageSource source) async {
  //   final pickedImage = await ImagePicker().pickImage(source: source);
  //   setState(() {
  //     if (pickedImage != null) {
  //       _imageFile = File(pickedImage.path);
  //     } else {
  //       print('No image selected.');
  //     }
  //   });
  //
  // }

  // Future<void> identifyInsect(String imagePath) async {
  //   const apiKey = 'rfJ0b2yUWWtr7RRUXkhNy4XO7K1UsUB5Je8qGz8Auc2XhCNRV3';
  //   const url = 'https://insect.mlapi.ai/api/v1';
  //
  //   // Read image file and encode as base64 string
  //   final bytes = await File(imagePath).readAsBytes();
  //   final base64Image = base64Encode(bytes);
  //
  //   // Create request body
  //   final body = {
  //     'images': [base64Image],
  //   };
  //
  //   // Send POST request
  //   final response = await http.post(
  //     Uri.parse(url),
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'X-Insect-Api-Key': apiKey,
  //     },
  //     body: jsonEncode(body),
  //   );
  //
  //   // Check if response is successful
  //   if (response.statusCode == 200) {
  //     // Parse response JSON
  //     final data = await jsonDecode(response.body);
  //
  //     // Set disease name
  //     setState(() {
  //       _diseaseName = data['predictions'][0]['species_common_name'];
  //     });
  //   } else {
  //     // Handle error
  //     print('Error: ${response.statusCode} ${response.reasonPhrase}');
  //   }
  // }



  void identifyPlant() async {
    // Read image file as bytes
    List<int> imageBytes = await File('unknown_plant.jpg').readAsBytes();

    // Encode image bytes to base64
    String base64Image = base64Encode(imageBytes);

    // Prepare the request payload
    Map<String, dynamic> payload = {
      "images": [base64Image],
      "modifiers": ["similar_images"],
      "plant_details": ["common_names", "url"],
    };
    //   const apiKey = 'rfJ0b2yUWWtr7RRUXkhNy4XO7K1UsUB5Je8qGz8Auc2XhCNRV3';
    //   const url = 'https://insect.mlapi.ai/api/v1';

    // Send the API request
    var response = await http.post(
      Uri.parse('https://api.plant.id/v2/identify'),
      headers: {
        'Content-Type': 'application/json',
        'Api-Key': 'rfJ0b2yUWWtr7RRUXkhNy4XO7K1UsUB5Je8qGz8Auc2XhCNRV3',
      },
      body: jsonEncode(payload),
    );

    // Process the API response
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List<dynamic> suggestions = data['suggestions'];

      for (var suggestion in suggestions) {
        print(suggestion['plant_name']);    // Taraxacum officinale
        print(suggestion['plant_details']['common_names']);    // ["Dandelion"]
        print(suggestion['plant_details']['url']);
      }
    } else {
      print('Failed to identify plant: ${response.statusCode}');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      appBar: AppBar(
        backgroundColor: mainColor,
        title: const Text('Disease Diagnosis'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: _imageFile.path.isNotEmpty,
              child: Image.file(
                _imageFile.path.isNotEmpty
                    ? _imageFile: File(''),
                width: 200,
                height: 200,
              ),
            ),
            const SizedBox(height: 10,),
            ElevatedButton(
              onPressed: () => _pickImage(ImageSource.camera),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text('Take a Photo', style: smallTextSize),
              ),
            ),
            const SizedBox(height: 10,),
            ElevatedButton(
              onPressed: () => _pickImage(ImageSource.gallery),
              child: Padding(
                padding:  const EdgeInsets.all(12.0),
                child: Text('Pick from Gallery', style: smallTextSize,),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
