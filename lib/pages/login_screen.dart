import 'package:agrogenius/pages/forgot_password_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../constants/text_constant.dart';
import '../widgets/alert_widget.dart';
import '../widgets/bottom_navigation_widget.dart';
import '../widgets/button_widget.dart';
import '../widgets/default_textfiled.dart';
import '../widgets/top_icon_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailCtrl = TextEditingController();
  final TextEditingController passwordCtrl = TextEditingController();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  bool isChecked = false;
  bool isLoading = false;

  @override
  void dispose() {
    emailCtrl.dispose();
    passwordCtrl.dispose();
    super.dispose();
  }

  Future<void> _login() async {
    if (emailCtrl.text.isEmpty) {
      CustomEdgeAlert.showErrorAlert(
        context: context,
        desc: 'Please enter your registered email',
      );
      return;
    }

    if (passwordCtrl.text.isEmpty) {
      CustomEdgeAlert.showErrorAlert(
        context: context,
        desc: 'Please enter your valid password',
      );
      return;
    }

    try {
      setState(() {
        isLoading = true;
      });

      final userCred = await firebaseAuth.signInWithEmailAndPassword(
        email: emailCtrl.text,
        password: passwordCtrl.text,
      );

      final user = userCred.user;

      if (user != null && user.email == emailCtrl.text) {
        if (context.mounted) {
          CustomEdgeAlert.showErrorAlert(
            context: context,
            desc: 'Login Successful!',
          );

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const CustomBottomNav()),
                (route) => false,
          );
        }
      } else {
        setState(() {
          isLoading = false;
        });

        if (context.mounted) {
          CustomEdgeAlert.showErrorAlert(
            context: context,
            desc: 'Login Failed! Please try again',
          );
        }
      }
    } catch (error) {
      setState(() {
        isLoading = false;
      });

      CustomEdgeAlert.showErrorAlert(
        context: context,
        desc: error.toString().split(']')[1].trim(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: ModalProgressHUD(
        progressIndicator: CircularProgressIndicator(
          color: mainColor,
        ),
        inAsyncCall: isLoading,
        child: SafeArea(
          child: Stack(
            children: [
              const TopIconWidget(),
              Center(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(bodyGapScrollView),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 2,
                        ),
                        width: double.maxFinite,
                        height: aboveImageHeight,
                        child: Image.asset(
                          'assets/images/login.png',
                        ),
                      ),
                      SizedBox(height: mediumHeightGap),
                      Text(
                        'Login to Agro Genius',
                        style: headlineTextSizeMedium,
                      ),
                      SizedBox(height: mediumHeightGap),
                      ReusableTextField(
                        prefixIcon: Icon(
                          Icons.email,
                          size: iconSize,
                        ),
                        controller: emailCtrl,
                        keyboardType: TextInputType.emailAddress,
                        hintText: 'Enter Email',
                        maxLines: 1,
                      ),
                      SizedBox(height: mediumHeightGap),
                      ReusableTextField(
                        prefixIcon: Icon(Icons.lock, size: iconSize,),
                        obscureText: true,
                          controller: passwordCtrl,
                          hintText: 'Enter password',
                      ),
                      SizedBox(height: longHeightGap),
                      CustomButtonWidget(
                        onPressed: _login,
                        child: Text(
                          'Login',
                          style: TextStyle(fontSize: buttonTextSize),
                        ),
                      ),
                      SizedBox(height: smallHeightGap),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                activeColor: mainColor,
                                value: isChecked,
                                onChanged: (newValue) {
                                  setState(() {
                                    isChecked = newValue ?? false;
                                  });
                                },
                              ),
                              Text(rememberMe),
                            ],
                          ),
                          TextButton(
                            child: Text(
                              forgotPassword,
                              style: TextStyle(color: textButtonColor),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const ForgotPasswordScreen(),
                                ),
                              );
                            },
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
