import 'package:agrogenius/Utils/day_selector_util.dart';
import 'package:agrogenius/constants/color_constants.dart';
import 'package:agrogenius/constants/size_constant.dart';
import 'package:agrogenius/pages/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:geolocator/geolocator.dart';
import '../Utils/uvindex_utils.dart';
import '../controller/weather_controller.dart';
import '../models/weather_data_model.dart';
import '../widgets/bottom_navigation_widget.dart';
import '../widgets/weather_details_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;
  int scheduleLength = 0;
  int articleLength = 0;
  bool isLoading = true;
  MyWeather? weatherData;
  late final uvIndexUtils = UvIndexUtils();


  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    FlutterNativeSplash.remove();
    locationTracking();
  }

  void locationTracking() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      getMyCurrentLocation(position.latitude, position.longitude);
    } catch (error) {
      print(error.toString());
      isLoading = false;
    }
  }

  void getMyCurrentLocation(double lat, double lon) async {
    MyWeather myWeather =
        await WeatherController().getMyCurrentWeatherCtl(lat, lon);
    if (myWeather.longitude != null) {
      setState(() {
        weatherData = myWeather;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }




  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: SafeArea(
            child: isLoading == false
                ? SingleChildScrollView(
                    child: Column(
                      children: [
                        Card(
                          elevation: elevation,
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          shadowColor: shadowColor,
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 15),
                            decoration: BoxDecoration(
                                // border: Border.all(width: 5, color: Colors.white),
                                color: bgColor2),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/${weatherData?.days![selectedIndex].icon}.png',
                                      height: 40,
                                      width: 40,
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      (weatherData!
                                              .days![selectedIndex].conditions)
                                          .toString()
                                          .toUpperCase(),
                                      style: const TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.blue),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 3),
                                      margin: const EdgeInsets.only(right: 5),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(borderRadius),
                                        color: Colors.blue,
                                      ),
                                      child: const Icon(Icons.thermostat,
                                          color: Colors.white),
                                    ),
                                    Text(
                                      '${weatherData!.days![selectedIndex].temp ?? ""}\u00B0C',
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 10, top: 0),
                          padding: const EdgeInsets.only(
                            top: 3,
                          ),
                          height: 90,
                          color: Colors.white,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              List<String> dayNames = GetDataDay.getDayNames();
                              return InkWell(
                                onTap: () {
                                  _onItemTapped(index);
                                },
                                child: Card(
                                  shadowColor: shadowColor,
                                  elevation: elevation,
                                  child: Container(
                                    width: 65,
                                    decoration: BoxDecoration(
                                      color: selectedIndex == index
                                          ? mainColor
                                          : bgColor2,
                                      borderRadius:
                                          BorderRadius.circular(borderRadius),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          dayNames[index].split(",")[1].trim(),
                                          style: TextStyle(
                                            fontSize: listViewFontSize,
                                            fontWeight: FontWeight.w500,
                                            color: selectedIndex == index
                                                ? whiteColor
                                                : Colors.black,
                                          ),
                                        ),
                                        Text(
                                          dayNames[index].split(",")[0].trim(),
                                          style: TextStyle(
                                            fontSize: listViewFontSize,
                                            fontWeight: FontWeight.w500,
                                            color: selectedIndex == index
                                                ? whiteColor
                                                : Colors.black,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: weatherData!.days!
                                .length, // the number of items you want to display
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          padding: const EdgeInsets.all(15),
                          height: 100, // or any height you want
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              WeatherCard(
                                icon: Icons.sunny,
                                title: 'Light',
                                value:
                                    '${((weatherData!.days![selectedIndex].solarradiation / 0.0079) / 1000).toStringAsFixed(1)}k lux',
                                iconBgColor: Colors.amber,
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              WeatherCard(
                                icon: Icons.air,
                                title: 'Wind',
                                value:
                                    '${weatherData!.days![selectedIndex].windspeed}m/h',
                                iconBgColor: Colors.lightGreen.shade600,
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              WeatherCard(
                                icon: Icons.water_drop,
                                title: 'Humidity',
                                value:
                                    '${weatherData!.days![selectedIndex].humidity}%',
                                iconBgColor: Colors.lightBlueAccent.shade100,
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              WeatherCard(
                                icon: Icons.wb_incandescent,
                                title: 'UV',
                                value: uvIndexUtils.getUVRiskLevel(
                                    weatherData!.days![selectedIndex].uvindex),
                                // '${weatherData!.days![_selectedIndex].uvindex}',
                                iconBgColor: Colors.purple.shade200,
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              WeatherCard(
                                icon: Icons.remove_red_eye,
                                title: 'Visibility',
                                value:
                                    '${(weatherData!.days![selectedIndex].visibility / 10).toStringAsFixed(1)} Km',
                                iconBgColor: Colors.blueGrey.shade200,
                              ),
                              const SizedBox(
                                width: 2,
                              ),
                              WeatherCard(
                                icon: Icons.timeline,
                                title: 'Air pressure',
                                value:
                                    '${(weatherData!.days![selectedIndex].pressure).round()} hPa',
                                iconBgColor: Colors.lightGreen.shade300,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 40,
                          // color: Colors.white,
                          margin: const EdgeInsets.only(top: 8),
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "Schedule",
                                    style: headlineTextSizeMedium,
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      // Handle button tap
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return const LoginScreen();
                                      }));
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.add,
                                          color: mainColor,
                                          size: 28,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          'Add Plant',
                                          style: TextStyle(color: mainColor),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        scheduleLength == 0
                            ? Container(
                                height: 230,
                                margin: const EdgeInsets.only(
                                    top: 0, left: 15, right: 15, bottom: 15),
                                decoration: BoxDecoration(color: bgColor),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      height: 15,
                                      right: 0,
                                      child: Container(
                                        color: Colors.white,
                                      ),
                                    ),
                                    Positioned(
                                      top: 0,
                                      right: 22,
                                      width: 88,
                                      child: Image.asset(
                                          'assets/images/greenarrow.png'),
                                    ),
                                    Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                              "assets/images/noplat.png"),
                                          Text(
                                            'You don\'t have any plants yet. Add your plant now',
                                            style: headlineTextSizeSmall,
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                margin:
                                    const EdgeInsets.only(left: 15, top: 10),
                                height: 210,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 5,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Card(
                                      margin: const EdgeInsets.only(
                                          top: 8,
                                          right: 10,
                                          left: 3,
                                          bottom: 5),
                                      elevation: elevation,
                                      // margin: const EdgeInsets.all(4),
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            top: 6,
                                            left: 7,
                                            right: 7,
                                            bottom: 4),
                                        width: 145,
                                        decoration: BoxDecoration(
                                            // border: Border.all(width: 1, color: const Color(0xFFe5e5e5)),
                                            borderRadius: BorderRadius.circular(
                                                borderRadius)),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  child: Image.asset(
                                                    "assets/images/dashboard.png",
                                                    width: double.maxFinite,
                                                    height: 80,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 3,
                                                ),
                                                Text(
                                                  'Tomatoes $index',
                                                  style: const TextStyle(
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 7,
                                                      horizontal: 2),
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 5,
                                                      horizontal: 8),
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueGrey,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              borderRadius)),
                                                  child: const Text(
                                                    "Button",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Container(
                                                      width: 25,
                                                      height: 25,
                                                      padding:
                                                          const EdgeInsets.all(
                                                              3),
                                                      decoration: BoxDecoration(
                                                        color: Colors.green,
                                                        // set the background color to green
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                borderRadius),
                                                      ),
                                                      child: const Icon(
                                                        Icons.local_florist,
                                                        color: Colors.white,
                                                        size: 17,
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      "3hr 4min",
                                                      style: smallTextSize,
                                                    )
                                                  ],
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                        Container(
                          height: 40,
                          margin: const EdgeInsets.only(top: 8),
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "Latest Articles",
                                    style: headlineTextSizeMedium,
                                  ),
                                ],
                              ),
                              TextButton(
                                onPressed: () {
                                  // _onItemTapped(2);
                                  // Navigator.pop(context);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const CustomBottomNav(
                                              feedIndex: 1),
                                    ),
                                  );
                                },
                                child: const Text('Show More'),
                              )
                            ],
                          ),
                        ),
                        articleLength == 0
                            ? Container(
                                height: 230,
                                margin: const EdgeInsets.only(
                                    top: 0, left: 15, right: 15, bottom: 15),
                                decoration: BoxDecoration(color: bgColor),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      height: 15,
                                      right: 0,
                                      child: Container(
                                        color: Colors.white,
                                      ),
                                    ),
                                    Positioned(
                                      top: 0,
                                      right: 22,
                                      width: 88,
                                      child: Image.asset(
                                          'assets/images/greenarrow.png'),
                                    ),
                                    Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                              "assets/images/noplat.png"),
                                          Text(
                                            'You don\'t have any plants yet. Add your plant now',
                                            style: headlineTextSizeSmall,
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                margin:
                                    const EdgeInsets.only(left: 15, top: 10),
                                height: 210,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 5,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Card(
                                      margin: const EdgeInsets.only(
                                          top: 8,
                                          right: 10,
                                          left: 3,
                                          bottom: 5),
                                      elevation: elevation,
                                      // margin: const EdgeInsets.all(4),
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            top: 6,
                                            left: 7,
                                            right: 7,
                                            bottom: 4),
                                        width: 145,
                                        decoration: BoxDecoration(
                                            // border: Border.all(width: 1, color: const Color(0xFFe5e5e5)),
                                            borderRadius: BorderRadius.circular(
                                                borderRadius)),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                  child: Image.asset(
                                                    "assets/images/dashboard.png",
                                                    width: double.maxFinite,
                                                    height: 80,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 3,
                                                ),
                                                Text(
                                                  'Tomatoes $index',
                                                  style: const TextStyle(
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 7,
                                                      horizontal: 2),
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 5,
                                                      horizontal: 8),
                                                  decoration: BoxDecoration(
                                                      color: Colors.blueGrey,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              borderRadius)),
                                                  child: const Text(
                                                    "Button",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Container(
                                                      width: 25,
                                                      height: 25,
                                                      padding:
                                                          const EdgeInsets.all(
                                                              3),
                                                      decoration: BoxDecoration(
                                                        color: Colors.green,
                                                        // set the background color to green
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                borderRadius),
                                                      ),
                                                      child: const Icon(
                                                        Icons.local_florist,
                                                        color: Colors.white,
                                                        size: 17,
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      "3hr 4min",
                                                      style: smallTextSize,
                                                    )
                                                  ],
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ],
                    ),
                  )
                : const Center(child: CircularProgressIndicator())));
  }
}
