import 'package:flutter/material.dart';

class ReportScreen extends StatefulWidget {
  const ReportScreen({Key? key}) : super(key: key);

  @override
  State<ReportScreen> createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  String? myName;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      myName = "ReportScreen \n ${DateTime.now().millisecondsSinceEpoch}";
    });

  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          child: Text("$myName", style: TextStyle(
              fontSize: 24
          ),)
      ),
    );
  }
}