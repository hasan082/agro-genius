import 'package:agrogenius/pages/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import '../Utils/internetdetection.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../widgets/alert_widget.dart';
import '../widgets/button_widget.dart';
import '../widgets/default_textfiled.dart';
import '../widgets/top_icon_widget.dart';
import 'nointernet.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController nameCtrl = TextEditingController();
  final TextEditingController emailCtrl = TextEditingController();
  final TextEditingController passwordCtrl = TextEditingController();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  bool _isLoading = false;
  bool hasInternetConnection = false;
  final InternetConnectionChecker internetChecker = InternetConnectionChecker();

  @override
  void initState() {
    super.initState();
    checkInternet();
  }

  Future<void> checkInternet() async {
    final result = await internetChecker.checkInternetConnection();
    setState(() {
      hasInternetConnection = result;
    });
  }

  void registerUser() async {
    setState(() {
      _isLoading = true;
    });

    try {
      UserCredential userCred = await firebaseAuth.createUserWithEmailAndPassword(
        email: emailCtrl.text,
        password: passwordCtrl.text,
      );
      User? user = userCred.user;

      if (user != null && user.email == emailCtrl.text) {
        if (context.mounted) {
          CustomEdgeAlert.showErrorAlert(
            context: context,
            desc: 'Registration Successful! Please login',
          );
          user.updateDisplayName(nameCtrl.text);
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) {
              return const LoginScreen();
            }),
                (route) => false,
          );
        }
      } else {
        if (context.mounted) {
          CustomEdgeAlert.showErrorAlert(
            context: context,
            desc: 'Registration failed!',
          );
        }
      }
    } catch (error) {
      CustomEdgeAlert.showErrorAlert(
        context: context,
        desc: error.toString().split(']')[1].trim(),
      );
    } finally {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: Stack(
            children: [
              const TopIconWidget(),
              Center(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(bodyGapScrollView),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 2,
                        ),
                        width: double.maxFinite,
                        height: aboveImageHeight,
                        child: Image.asset(
                          'assets/images/login.png',
                        ),
                      ),
                      SizedBox(height: mediumHeightGap),
                      Text(
                        'Register to Agro Genius',
                        style: headlineTextSizeMedium,
                      ),
                      SizedBox(height: mediumHeightGap),
                      ReusableTextField(
                        prefixIcon:  Icon(
                          Icons.person,
                          size: iconSize,
                        ),
                        controller: nameCtrl,
                        hintText: 'Enter Name',
                        maxLines: 1,
                      ),
                      SizedBox(height: mediumHeightGap),
                      ReusableTextField(
                        prefixIcon: Icon(
                          Icons.email,
                          size: iconSize,
                        ),
                        controller: emailCtrl,
                        keyboardType: TextInputType.emailAddress,
                        hintText: 'Enter Email',
                        maxLines: 1,
                      ),
                      SizedBox(height: mediumHeightGap),
                      ReusableTextField(
                        prefixIcon: Icon(
                          Icons.lock,
                          size: iconSize,
                        ),
                        obscureText: true,
                        controller: passwordCtrl,
                        hintText: 'Enter Password',
                      ),
                      SizedBox(height: longHeightGap),
                      CustomButtonWidget(
                        child: Text(
                          'Register',
                          style: TextStyle(fontSize: buttonTextSize),
                        ),
                        onPressed: () {
                          if (hasInternetConnection) {
                            if (nameCtrl.text.isEmpty) {
                              CustomEdgeAlert.showErrorAlert(
                                context: context,
                                desc: 'Please enter your name',
                              );
                            } else if (emailCtrl.text.isEmpty) {
                              CustomEdgeAlert.showErrorAlert(
                                context: context,
                                desc: 'Please enter your valid email',
                              );
                            } else if (passwordCtrl.text.trim().length < 6) {
                              CustomEdgeAlert.showErrorAlert(
                                context: context,
                                desc: 'Please enter your 6 digit password',
                              );
                            } else {
                              registerUser();
                            }
                          } else {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return const NoInternetScreen();
                              }),
                            );
                          }
                        },
                      ),
                      SizedBox(height: smallHeightGap),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Already have an account?'),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) {
                                  return const LoginScreen();
                                }),
                              );
                            },
                            child: Text(
                              'Login Now',
                              style: TextStyle(fontSize: 14, color: mainColor),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
