import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import '../constants/color_constants.dart';

class UpdateUserProfileInfo extends StatefulWidget {
  const UpdateUserProfileInfo({Key? key}) : super(key: key);

  @override
  State<UpdateUserProfileInfo> createState() => _UpdateUserProfileInfoState();
}

class _UpdateUserProfileInfoState extends State<UpdateUserProfileInfo> {
  final TextEditingController _name = TextEditingController();
  final TextEditingController _phoneNumber = TextEditingController();
  final TextEditingController _dob = TextEditingController();
  File? _imageFile;






  Future<String> _postData(String name, String phone, String dob, String imgurl) async {
    const url = 'https://hasan-as-developer.000webhostapp.com/agrogenious/data.php'; // replace with your PHP script URL
    final response = await http.post(Uri.parse(url), body: {
      'name': name,
      'phone': phone,
      'dob': dob,
      'imgurl': imgurl,
    });
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception('Failed to post data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Update Info'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.check),
            onPressed: () async {
              await _postData(_name.text, _phoneNumber.text, _dob.text, _imageFile?.path ?? '');
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: [
                Container(
                  padding: const EdgeInsets.all(5.0),
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    border: Border.all(
                      color: mainColor,
                      width: 3,
                    ),
                  ),
                  child: CircleAvatar(
                    radius: 60,
                    backgroundImage: const AssetImage('assets/images/profile.jpg'),
                  ),
                ),
                Positioned(
                  top: 30,
                  right: -10,
                  child: IconButton(
                    padding: const EdgeInsets.all(5),
                    constraints: const BoxConstraints(
                      minHeight: 20,
                      minWidth: 20,
                    ),
                    onPressed: () {
                      // _showChoiceDialog(context);
                    },
                    icon: const Icon(Icons.edit),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            const Text(
              'Name:',
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 5.0),
            TextField(
              controller: _name,
              decoration: const InputDecoration(
                hintText: 'Enter your name',
              ),
            ),
            const SizedBox(height: 10.0),
            const Text(
              'Phone Number:',
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 5.0),
            TextField(
              decoration: const InputDecoration(
                hintText: 'Enter your phone number',
              ),
              controller: _phoneNumber,
            ),
            const SizedBox(height: 20.0),
            const Text(
              'Date of Birth:',
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            TextField(
              decoration: const InputDecoration(
                hintText: 'Date of Birth',
              ),
              controller: _dob,
            ),
          ],
        ),
      ),
    );
  }
}