import 'package:flutter/material.dart';

class FeedScreen extends StatefulWidget {
  const FeedScreen({Key? key}) : super(key: key);

  @override
  State<FeedScreen> createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  String? myName;
  @override
  void initState() {
    super.initState();
    setState(() {
      myName = "FeedScreen \n ${DateTime.now().millisecondsSinceEpoch}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          child: Text("$myName", style: const TextStyle(
              fontSize: 24
          ),)
      ),
    );
  }
}
