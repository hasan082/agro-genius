import 'package:agrogenius/pages/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import '../constants/size_constant.dart';
import '../constants/text_constant.dart';
import '../widgets/alert_widget.dart';
import '../widgets/button_widget.dart';
import '../widgets/default_textfiled.dart';
import '../widgets/top_icon_widget.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final TextEditingController emailCtrl = TextEditingController();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Future<void> resetPassword() async {
    try {
      if (emailCtrl.text.isEmpty) {
        CustomEdgeAlert.showErrorAlert(
          context: context,
          desc: 'Please enter your registered email',
        );
      } else {
        await firebaseAuth.sendPasswordResetEmail(email: emailCtrl.text);
        if(context.mounted){
          CustomEdgeAlert.showErrorAlert(
            context: context,
            desc: 'Please check ${emailCtrl.text} to reset your password',
          );
        }
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        CustomEdgeAlert.showErrorAlert(
          context: context,
          desc: 'No user found with email address ${emailCtrl.text}',
        );
      } else {
        CustomEdgeAlert.showErrorAlert(
          context: context,
          desc: e.message.toString().split(']')[1].trim(),
        );
      }
    } catch (e) {
      CustomEdgeAlert.showErrorAlert(
        context: context,
        desc: e.toString().split(']')[1].trim(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Stack(
          children: [
            const TopIconWidget(),
            Center(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(bodyGapScrollView),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 2,
                      ),
                      width: double.maxFinite,
                      height: aboveImageHeight,
                      child: Image.asset(
                        'assets/images/forgotpassword.png',
                      ),
                    ),
                    SizedBox(height: mediumHeightGap),
                    Text(forgotPageText, style: mediumTextSize),
                    SizedBox(height: mediumHeightGap),
                    ReusableTextField(
                      prefixIcon: Icon(Icons.email, size: iconSize),
                      controller: emailCtrl,
                      keyboardType: TextInputType.emailAddress,
                      hintText: 'Enter Email',
                      maxLines: 1,
                    ),
                    SizedBox(height: mediumHeightGap),
                    CustomButtonWidget(
                      onPressed: resetPassword,
                      child: Text(
                        'Get Your New Password',
                        style: TextStyle(fontSize: buttonTextSize),
                      ),
                    ),
                    SizedBox(height: smallHeightGap),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Already have an account?'),
                        TextButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return const LoginScreen();
                            }));
                          },
                          child: Text(
                            'Login Now',
                            style: TextStyle(fontSize: 14, color: mainColor),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
