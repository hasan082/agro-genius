import 'package:edge_alerts/edge_alerts.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';

class CustomEdgeAlert {
  static void showErrorAlert({
    required BuildContext context,
    required String desc,
  }) {
    edgeAlert(
      context,
      duration: 1,
      description: desc,
      gravity: Gravity.top,
      icon: Icons.error_outline_sharp,
      backgroundColor: mainColor,
    );
  }
}
