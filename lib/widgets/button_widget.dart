import 'package:flutter/material.dart';

import '../constants/color_constants.dart';

class CustomButtonWidget extends StatelessWidget {
  final Function()? onPressed;
  final Widget? child;

  const CustomButtonWidget({super.key, this.onPressed, this.child});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 7.0,
      borderRadius: BorderRadius.circular(6.0),
      color: mainColor,
      child: MaterialButton(
        onPressed: onPressed,
        minWidth: double.maxFinite,
        height: 45.0,
        textColor: Colors.white,
        child: child,
      ),
    );
  }
}
