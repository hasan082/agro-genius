import 'package:flutter/material.dart';

class TopIconWidget extends StatelessWidget {
  const TopIconWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 15,
          right: 15,
          child: Image.asset(
            'assets/images/Leaf_icon.png',
            width: 50,
            height: 50,
          ),
        ),
        Positioned(
          top: 15,
          left: 15,
          child: Image.asset(
            'assets/images/Leaf_icon.png',
            width: 50,
            height: 50,
          ),
        ),
      ],
    );
  }
}
