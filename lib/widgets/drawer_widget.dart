import 'package:agrogenius/pages/login_screen.dart';
import 'package:agrogenius/pages/profile_screen.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';


class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final drawerWidth = screenWidth < 600 ? 300.0 : screenWidth;
    return Drawer(
      width: drawerWidth,
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: const Text('User Person Name'),
            currentAccountPicture: CircleAvatar(
              child: Image.asset(
                'assets/images/login.png',
              ),
            ),
            accountEmail: const Text('User Email'),
            decoration: BoxDecoration(
              color: mainColor,
            ),
          ),
          buildDrawerItem(
            title: 'Page 1',
            icon: Icons.arrow_forward_rounded,
            onTap: () {
              navigateToPage(context, const ProfileScreen());
            },
          ),
          buildDrawerItem(
            title: 'Page 2',
            icon: Icons.arrow_forward_rounded,
            onTap: () {

            },
          ),
          buildDrawerItem(
            title: 'Logout',
            icon: Icons.arrow_forward_rounded,
            onTap: () {
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context){
                return const LoginScreen();
              }), (route) => false);
            },
          ),
        ],
      ),
    );
  }

  Widget buildDrawerItem({
    required String title,
    required IconData icon,
    required VoidCallback onTap,
  }) {
    return ListTile(
      title: Text(title),
      trailing: Icon(icon),
      onTap: onTap,
    );
  }

  void navigateToPage(BuildContext context, Widget page) {
    Navigator.push(context, MaterialPageRoute(builder: (_) => page));
  }
}
