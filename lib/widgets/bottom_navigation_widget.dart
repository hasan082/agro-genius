import 'package:agrogenius/pages/plant_screen.dart';
import 'package:agrogenius/pages/feed_screen.dart';
import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import '../constants/color_constants.dart';
import '../pages/image_pick_screen.dart';
import '../pages/profile_screen.dart';
import '../pages/report_screen.dart';
import 'drawer_widget.dart';

class CustomBottomNav extends StatefulWidget {
  final int? feedIndex;
  final String? imageUrl;

  const CustomBottomNav({Key? key, this.feedIndex, this.imageUrl}) : super(key: key);

  @override
  State<CustomBottomNav> createState() => _CustomBottomNavState();
}

class _CustomBottomNavState extends State<CustomBottomNav> {
  int _selectedIndex = 0;

  @override
  void initState() {
    if(widget.feedIndex != null){
      _selectedIndex = widget.feedIndex!;
    }
    super.initState();
  }

  static const List<_PageData> _pageData = [
    _PageData(page: HomePage(), icon: Icons.energy_savings_leaf_outlined, title: 'Plant'),
    _PageData(page: FeedScreen(), icon: Icons.article, title: 'Feed'),
    _PageData(page: ReportScreen(), icon: Icons.bar_chart, title: 'Report'),
    _PageData(page: ProfileScreen(), icon: Icons.person, title: 'Profile'),
  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      drawer: const CustomDrawer(),
      body: IndexedStack(
        index: _selectedIndex,
        children: _pageData.map((data) => data.page).toList(),
      ),
      bottomNavigationBar: AnimatedBottomNavigationBar(
        backgroundColor: Colors.white,
        splashColor: mainColor,
        icons: _pageData.map((data) => data.icon).toList(),
        notchSmoothness: NotchSmoothness.defaultEdge,
        activeColor: mainColor,
        inactiveColor: textColor,
        activeIndex: _selectedIndex,
        elevation: 8,
        gapLocation: GapLocation.center,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
      floatingActionButton: SizedBox(
        width: 55,
        height: 55,
        child: FloatingActionButton(
          highlightElevation: 20,
          elevation: 0,
          backgroundColor: mainColor,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return const ImagePickScreen();
            }));
          },
          child: Image.asset('assets/images/floatingbutton.png'),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            _pageData[_selectedIndex].title,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 24,
            ),
          ),
          const Icon(Icons.notifications, color: Colors.white, size: 30.0),
        ],
      ),
      backgroundColor: mainColor,
      elevation: 0.0,
    );
  }
}

class _PageData {
  final Widget page;
  final IconData icon;
  final String title;

  const _PageData({
    required this.page,
    required this.icon,
    required this.title,
  });
}
