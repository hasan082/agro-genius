import 'package:agrogenius/pages/onboarding_screen.dart';
import 'package:agrogenius/widgets/bottom_navigation_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'constants/color_constants.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Firebase.initializeApp();
  runApp(AgroGenius());
}


class AgroGenius extends StatelessWidget {
   AgroGenius({super.key});

  static Color customColor = mainColor;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // This widgets is the root of your application.
  @override
  Widget build(BuildContext context) {
    User? user = _auth.currentUser;
    return MaterialApp(
      builder: (context, child) {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);
        return child!;
      },
      debugShowCheckedModeBanner: false,
      title: 'Agro Genius',
      theme: ThemeData().copyWith(
        colorScheme: ThemeData().colorScheme.copyWith(
          primary: mainColor,
        ),
        textTheme: TextTheme(
          titleLarge:TextStyle(
            color: textColor,
          ),
          titleMedium:TextStyle(
            color: textColor,
          ),
          titleSmall:TextStyle(
            color: textColor,
          ),
          bodyLarge: TextStyle(
            color: textColor,
          ),
          bodyMedium: TextStyle(
            color: textColor,
          ),
          bodySmall: TextStyle(
            color: textColor,
          ),
        )
      ),
      // home: const OnBoardingScreen(),
      home: user != null ? const CustomBottomNav() : const OnBoardingScreen()

      // const OnBoardingScreen(),
    );
  }
}
