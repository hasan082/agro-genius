import 'dart:convert';
import 'package:agrogenius/repository/weather_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import '../models/weather_data_model.dart';

class WeatherController {
  final WeatherRepository weatherRepository = WeatherRepository();
  Future<MyWeather> getMyCurrentWeatherCtl(double lat, double lon) async {
    MyWeather myWeather = MyWeather();
    try{
      Response response = await weatherRepository.getMyCurrentWeatherRepo(lat, lon);
      myWeather = MyWeather.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    }catch(error){
      if (kDebugMode) {
        print(error.toString());
      }
    }
    return myWeather;
  }


}