## Agro Genius

Agro Genius is a cutting-edge mobile app built with Flutter that aims to revolutionize the agricultural industry. It provides farmers with advanced tools and features to optimize crop management, access vital information, and foster collaboration within the farming community.

### Features

- **Intuitive Interface**: User-friendly design for easy navigation and a seamless user experience.
- **MVC Architecture**: Follows the Model-View-Controller pattern for a robust and scalable code structure.
- **Provider State Management**: Efficiently manages and updates app state for smooth performance.
- **Splash Screen**: Engaging introductory screen that sets the tone for the app experience.
- **Onboarding**: Informative screens to guide users and familiarize them with the app's functionalities.
- **Firebase Authentication**: Secure user authentication and personalized account access.
- **Image Picker**: Capture and upload images for analysis and diagnosis of plant diseases.
- **Real-time Weather API**: Provides up-to-date weather information crucial for informed decision-making in farming practices.
- **Plant Disease Diagnosis API**: Offers comprehensive plant information and actionable recommendations for treatment.
- **Schedule Widget**: Helps farmers manage plant schedules, ensuring timely actions and improved crop health.
- **Article Posting**: Users can share their experiences, growth updates, and challenges with the farming community.
- **Detailed Reports**: Provides valuable data and analytics on plant growth and earnings for informed decision-making.
- **User Profiles**: Personalize profiles, manage data, and display profile pictures.
- **Community Engagement**: Explore and engage with other farmers' posts, fostering collaboration and knowledge exchange.
- **Notification/Alarm System**: Timely alerts and reminders to ensure important tasks are not missed.
- **Bottom Navigation**: Enables easy navigation between app sections for a seamless user experience.
- **Center Doc Floating Button**: Provides quick access to essential functions and actions, including image picking for plant disease diagnosis.

### Flutter Packaged Used

- firebase_core: ^2.5.0
- cloud_firestore: ^4.3.2
- firebase_auth: ^4.2.6
- firebase_storage: ^11.0.11
- firebase_database: ^10.0.10
- modal_progress_hud_nsn: ^0.4.0
- edge_alerts: ^0.0.1
- connectivity_plus: ^4.0.1
- geolocator: ^9.0.2
- image_picker: ^0.8.5
- http: ^0.13.6
- permission_handler: ^10.2.0
- shared_preferences: ^2.0.18
- animated_bottom_navigation_bar: ^1.2.0
- page_transition: ^2.0.9
- flutter_native_splash: ^2.3.0

### API used in this project

- https://weather.visualcrossing.com
- https://mlapi.ai/ (insect identification API.)



### Installation and Setup

1. Clone the repository.
2. Install the required dependencies using `flutter pub get`.
3. Set up your Firebase project and obtain the necessary configuration files.
4. Configure the project to connect with the Real-time Weather API and Plant Disease Diagnosis API.
5. Build and run the app on your preferred Flutter-supported platform.

### Usage

1. Launch the Agro Genius app.
2. Create an account or log in using your existing credentials.
3. Explore the intuitive interface and familiarize yourself with the available features.
4. Utilize the image picker to capture and upload images for plant disease diagnosis.
5. Access real-time weather information to make informed decisions about farming practices.
6. Manage your plant schedules using the Schedule widget for optimal crop health.
7. Share your experiences, growth updates, and challenges with the farming community through article posting.
8. Monitor detailed reports on plant growth and earnings to drive informed decision-making.
9. Engage with other farmers, view their posts, and foster collaboration within the Agro Genius community.
10. Stay updated with timely notifications and alerts to ensure critical tasks are not missed.

### Contributing

Contributions to Agro Genius are welcome! If you have any ideas, bug fixes, or feature requests, please submit them via pull requests. Make sure to follow the project's coding conventions and include tests for any new functionality.

### License

This project is licensed under the [[MIT License](./LICENSE)]

### Acknowledgements

We would like to express our gratitude to the open-source community for their invaluable contributions and support.

### Contact



For any inquiries or further information, please reach out to us at dr.has82@gmail.com.

Feel free to modify and enhance this structure based on your specific requirements and preferences. Remember to include any additional sections that may be relevant to your project, such as troubleshooting, frequently asked questions (FAQs), or deployment instructions.


## Tags
- Agriculture
- Farming
- Crop Management
- Agriculture 
- Farming
- Crop Management 
- Mobile App 
- Flutter 
- AgTech 
- Plant Disease Diagnosis 
- Weather Information 
- Community 
- Farming Tools


![Screeshot](agrolayout.png)